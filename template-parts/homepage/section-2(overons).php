

    <div class="section section-2 section-simple-content">
        <div class="container">
            <div class="col-10 col-md-8 col-lg-5 mx-auto">
                <?php if( get_field('title_sec2', 'option') && get_field('button_sec2', 'option') ) : ?>
                    <div class="content-set centered">
                        <h2 class="title">
                            <?php the_field('title_sec2', 'option'); ?>
                        </h2>
                        <div class="content theme-wp-content">
                            <?php the_field('content_sec2', 'option'); ?>
                        </div>
                        <a href="<?php the_field('button_sec2', 'option'); ?>" class="btn outline black mx-auto"><?php _e('Lees meer', 'nakedlaser'); ?></a>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>