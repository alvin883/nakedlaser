
    <?php if(get_field('title_sec6', 'option') && get_field('tagline_sec6', 'option')): ?>
        <div class="section section-6">
            <div class="container">
                <div class="content-set centered">
                    <h2 class="title">
                        <?php the_field('title_sec6', 'option'); ?>
                    </h2>
                </div>
                <div class="row">
                    <div class="col-12 col-md-6 left">
                        <img src="<?php echo get_stylesheet_directory_uri() . "/src/img/section-6.png"; ?>" class="the-img" />
                    </div>
                    <div class="col-12 col-md-6 row-content">
                        <div class="item">
                            <h4 class="title">
                                <?php the_field('tagline_sec6', 'option'); ?>
                            </h4>
                            <div class="content">
                                <ul>
                                    <?php if(have_rows('breakdown_sec6', 'option')) :
                                        while(have_rows('breakdown_sec6', 'option')) : the_row(); ?>
                                            <li>
                                                <?php the_sub_field('breakdown_point', 'option'); ?>
                                            </li>
                                        <?php endwhile ; 
                                    endif ; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?> 