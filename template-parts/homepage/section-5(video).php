
    <?php if(get_field('title_sec5', 'option') && get_field('video_sec5', 'option')) : ?>
        <div class="section section-5">
            <svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="1366px" height="81px" version="1.1" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd"
                viewBox="0 0 584 35"
                xmlns:xlink="http://www.w3.org/1999/xlink"
                class="the-wave"
                preserveAspectRatio="none">
                <defs>
                </defs>
                <g id="Layer_x0020_1">
                <metadata id="CorelCorpID_0Corel-Layer"/>
                <path class="fil0 the-wave-path" d="M584 13l0 22 -584 0c0,-39 0,17 0,-22 95,-17 192,-17 292,0 98,19 195,19 292,0z"/>
                </g>
            </svg>
            <div class="container">
                <div class="content-set">
                    <h2 class="title">
                        <?php the_field('title_sec5', 'option'); ?>
                    </h2>
                    <a class="btn circle mx-auto play-video" data-text="Video Afspelen"></a>
                </div>
            </div>
        </div>
        
        <div class="section-5-popup">
            <div class="wrapper">
                <button class="btn circle white close-button">
                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 47.971 47.971" xml:space="preserve" class="icon-menu icon-menu-close">
                        <path d="M28.228,23.986L47.092,5.122c1.172-1.171,1.172-3.071,0-4.242c-1.172-1.172-3.07-1.172-4.242,0L23.986,19.744L5.121,0.88
                            c-1.172-1.172-3.07-1.172-4.242,0c-1.172,1.171-1.172,3.071,0,4.242l18.865,18.864L0.879,42.85c-1.172,1.171-1.172,3.071,0,4.242
                            C1.465,47.677,2.233,47.97,3,47.97s1.535-0.293,2.121-0.879l18.865-18.864L42.85,47.091c0.586,0.586,1.354,0.879,2.121,0.879
                            s1.535-0.293,2.121-0.879c1.172-1.171,1.172-3.071,0-4.242L28.228,23.986z"></path>
                    </svg>
                </button>

                <?php
                    $video = get_field('video_sec5','option');
                    if( count(explode('enablejsapi=1', $video)) === 1 ) {
                        $pattern = '/src=".*?"/m';
                        preg_match( $pattern, $video, $video_arr );
                        $new_src = substr($video_arr[0], 0, -1);
                        $new_src .= '?enablejsapi=1&autoplay=1"';

                        $video = preg_replace( $pattern, $new_src, $video);
                    }
                ?>
                <script>
                    var videoiFrame = '<?php echo $video;?>';
                </script>
            </div>
        </div>
    <?php endif ; ?>