
    <?php if(get_field('timeline_sec4', 'option') && get_field('title1_sec4', 'option') && get_field('button_sec4', 'option')): ?> 
        <div class="section section-4">
            <div class="container">
                <div class="col-10 col-md-8 col-lg-5 mx-auto">
                    <div class="content-set centered">
                        <h2 class="title">
                            <?php the_field('title1_sec4', 'option'); ?>
                        </h2>
                        <p class="content">
                            <?php the_field('subtitle_sec4', 'option'); ?>
                        </p>
                    </div>
                </div>
                <div class="timeline">
                    <div class="line"></div>
                    <div class="items">
                        <?php if( have_rows('timeline_sec4', 'option') ) :
                            while(have_rows('timeline_sec4', 'option')) : the_row(); ?>
                                <div class="item">
                                    <div class="head">
                                        <h3 class="title">
                                            <?php the_sub_field('title2_sec4', 'option'); ?>
                                        </h3>
                                        <p class="content">
                                            <?php the_sub_field('content_sec4', 'option'); ?>
                                        </p>
                                    </div>
                                </div> 
                            <?php endwhile ;
                        endif ; ?>
                    </div>
                </div>
                <div class="end-button">
                    <a href="<?php the_field('button_sec4', 'option'); ?>" class="btn"><?php _e('Lees meer over', 'nakedlaser'); ?> <b>Soprana Ice Laser</b></a>
                </div>
            </div>
        </div>
    <?php endif ; ?>