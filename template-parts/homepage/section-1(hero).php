

    <div class="section section-1">
        <div class="slick-loading">
            Loading ...
        </div>
        <div class="right">
            <img src="<?php echo get_stylesheet_directory_uri() . "/src/img/legs-round.png"; ?>" class="test-img" />
        </div>
        <div class="container">
            <div class="left">
                <div class="slider">
                    <?php if(have_rows('slider_sec1', 'option')) : 
                        while(have_rows('slider_sec1', 'option')) : the_row(); ?>
                            <div class="content-set">
                                <h1 class="title">
                                    <?php the_sub_field('title_sec1', 'option'); ?>
                                </h1>
                                <p class="content">
                                    <?php the_sub_field('subtitle_sec1', 'option'); ?>
                                </p>
                                <a href="<?php the_sub_field('button_sec1', 'option'); ?>" class="btn outline"><?php _e('Lees meer', 'nakedlaser'); ?></a>
                            </div>
                        <?php endwhile ; 
                    endif ; ?>
                </div>
                <div class="dots">
                    <?php $sum = count(get_field('slider_sec1', 'option')); 
                    $i = 0;
                    while($i < $sum) : 
                    ?>
                        <button class= "<?php echo 'dot-index-'. $i . ' ' . (( $i==0) ? 'active' : ' ') ; ?>"></button>
                        <?php $i++;?>
                    <?php endwhile ; ?>
                </div>
            </div>
        </div>
    </div>