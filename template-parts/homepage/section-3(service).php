
    <?php if(get_field('breakdown_sec3', 'option') && get_field('title1_sec3', 'option')) : ?>
        <div class="section section-3">
            <div class="container">
                <div class="content-set centered">
                    <h2 class="title">
                        <?php the_field('title1_sec3', 'option'); ?>
                    </h2>
                </div>
                <?php if(have_rows('breakdown_sec3', 'option')) : ?>
                    <div class="row">
                        <div class="col-12 col-lg-6 column-row service-slider slider-photo-wrapper">
                            <?php while(have_rows('breakdown_sec3', 'option')) : the_row(); ?>
                                <div class="slider-photo">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="item">
                                                <!-- <div class="title">
                                                    <?php _e('vooraf', 'nakedlaser'); ?>
                                                </div> -->
                                                <?php $image = get_sub_field('before_image', 'option'); ?>
                                                <div class="photo" style="background-image: url('<?php echo $image['url'];  ?>');"></div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="item">
                                                <!-- <div class="title">
                                                    <?php _e('achter', 'nakedlaker'); ?>
                                                </div> -->
                                                <?php $image = get_sub_field('after_image', 'option'); ?>
                                                <div class="photo" style="background-image: url('<?php echo $image['url'];  ?>');"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endwhile ; ?>
                        </div>
                        <div class="col-12 col-lg-6 column-content">
                            <div class="service-slider slider-content-wrapper">
                                <?php while(have_rows('breakdown_sec3', 'option')) : the_row(); ?>
                                        <div class="slider-content">
                                            <div class="wrapper">
                                                <h3 class="title">
                                                    <?php the_sub_field('title2_sec3', 'option'); ?>
                                                </h3>
                                                <div class="content">
                                                    <ul>
                                                        <?php if(have_rows('content_sec3_breakdown')) :
                                                            while(have_rows('content_sec3_breakdown')) : the_row(); ?>
                                                                <li>
                                                                    <?php the_sub_field('content_point_sec3', 'option'); ?> 
                                                                </li>
                                                            <?php endwhile ; 
                                                        endif ; ?>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endwhile ; ?>
                            </div>
                                
                            <div class="wrapper-button">
                                <button class="btn circle outline white prev"><i class="fas fa-chevron-left"></i></button>
                                <button class="btn circle outline white next"><i class="fas fa-chevron-right"></i></button>
                                <div class="service-slider slider-button-wrapper">
                                    <?php while(have_rows('breakdown_sec3', 'option')) : the_row(); ?>
                                            <!-- slider item - 1-->
                                            <div>
                                                <button class="btn white outline"><a href="<?php the_sub_field('button_sec3', 'option'); ?>"><?php the_sub_field('text_button_sec3', 'option'); ?></a></button>
                                            </div>
                                        <?php endwhile ;?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif ;?>
            </div><!-- . container -->
        </div>
    <?php endif ; ?>