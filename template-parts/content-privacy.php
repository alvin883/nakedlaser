<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * Template Name: Legals Page
 * @package NakedLaser
 */
get_header();
 
if(have_posts()) :
   while (have_posts()) : the_post(); ?>

       <div id="content">
           <div id="privacy">
               
               <div class="section header-privacy">
                   <div class="container container-content">
                       <div class="content-set">
                           <h1 class="title">
                               <?php the_title(); ?>
                           </h1>
                           <div>
                               Last updated : <?php echo get_post_modified_time("d F Y", null, null, true); ?>
                           </div>
                       </div>
                   </div>
               </div>

               <div class="section section-the-content">
                   <div class="container">
                       <div class="row">
                           <div class="col-12 col-md-8 mx-auto" id="the-content">
                               <?php the_content(); ?>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>

<?php 
   endwhile;
endif;

get_footer();