
                <?php if(get_field('title_booking_service', 'option') && get_field('refers_page_service', 'option')) : ?>
                    <div class="section section-cta">
                        <div class="container">
                            <div class="col-12 col-md-8 col-lg-6 mx-auto">
                                <div class="content-set">
                                    <h3 class="title">
                                        <?php the_field('title_booking_service', 'option'); ?>
                                    </h3>
                                    <div class="content">
                                        <?php the_field('content_booking_service', 'option'); ?>
                                    </div>
                                    <a href="<?php the_field('refers_page_service', 'option'); ?>" target="_blank" class="btn white mx-auto">
                                        <?php the_field('text_service', 'option'); ?>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif ; ?>