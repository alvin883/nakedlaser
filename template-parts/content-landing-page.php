<?php
/**
 * Template part for displaying list of all Services Page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * Template Name: Laser Ontharing Page
 * @package NakedLaser
 */

 get_header();
 
 if(have_posts()) :
    while (have_posts()) : the_post(); ?>

        <div id="content">
            <div id="landing">
                
                <div class="section full-thumbnail">
                    <div class="bg-image" <?php if( has_post_thumbnail() ){ 
                            echo 'style="background-image: url(\'' . get_the_post_thumbnail_url() . '\')"';
                        } ?>></div>    
                    <svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="1366px" height="81px" version="1.1" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd"
                        viewBox="0 0 584 35"
                        xmlns:xlink="http://www.w3.org/1999/xlink"
                        class="the-wave the-wave-1"
                        preserveAspectRatio="none">
                        <defs>
                        </defs>
                        <g id="Layer_x0020_1">
                        <metadata id="CorelCorpID_0Corel-Layer"/>
                        <path class="the-wave-path" d="M584 13l0 22 -584 0c0,-39 0,17 0,-22 95,-17 192,-17 292,0 98,19 195,19 292,0z"/>
                        <rect x="0" y="40" width="100%" height="100" />
                        </g>
                    </svg>

                    <div class="container container-content">
                        <div class="col-12 col-md-8 col-lg-6 mx-auto">
                            <div class="content-set">
                                <h1 class="title">
                                    <?php the_title(); ?>
                                </h1>
                                <div class="content theme-wp-content">
                                    <?php the_field('subtitle_hdr_service'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <?php if(get_field('title_what') && get_field('content_what')) : ?>
                    <div class="section section-simple-content">
                        <div class="container">
                            <div class="col-12 col-md-8 col-lg-6 mx-auto">
                                <div class="content-set centered">
                                    <h2 class="title">
                                        <?php the_field('title_what'); ?>
                                    </h2>
                                    <div class="content">
                                        <?php the_field('content_what'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif ; ?>

                <div class="section landing-point">
                    <svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="1366px" height="81px" version="1.1" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd"
                        viewBox="0 0 584 35"
                        xmlns:xlink="http://www.w3.org/1999/xlink"
                        class="the-wave the-wave-1"
                        preserveAspectRatio="none">
                        <defs>
                        </defs>
                        <g id="Layer_x0020_1">
                        <metadata id="CorelCorpID_0Corel-Layer"/>
                        <path class="the-wave-path" d="M584 13l0 22 -584 0c0,-39 0,17 0,-22 95,-17 192,-17 292,0 98,19 195,19 292,0z"/>
                        <rect x="0" y="40" width="100%" height="100" />
                        </g>
                    </svg>

                    <?php if(get_field('content_why') && get_field('breakdown_points')) : ?>
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-lg-5">
                                <div class="item">
                                    <div class="content-set">
                                        <h3 class="title">
                                            <?php the_field('title_why'); ?>
                                        </h3>
                                        <div class="content theme-wp-content">
                                            <?php the_field('content_why'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-lg-7">
                                <div class="item">
                                    <ul>
                                        <?php if(have_rows('breakdown_points')): 
                                            while(have_rows('breakdown_points')) : the_row(); ?>
                                                <li><?php the_sub_field('the_point'); ?>
                                            <?php endwhile ;
                                        endif ; ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endif ; ?>
                </div>
                
                <?php if(get_field('time_shav') && get_field('contactus_button')) : ?>
                    <div class="section landing-versus">
                        <div class="container">
                            <div class="content-set centered">
                                <h2 class="title">
                                    <?php _e('<b>Soprano</b> vs Shaving', 'nakedlaser'); ?>
                                </h2>
                                <div class="versus">
                                    <!-- Data item 1 -->
                                    <div class="point">
                                        <div class="logo">
                                            <i class="fas fa-stopwatch"></i>
                                        </div>
                                        <div class="item">
                                            <div class="box">
                                                <?php the_field('time_sop'); ?>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="box">
                                                <?php the_field('time_shav'); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End of data 1 -->

                                    <!-- Data item 2 -->
                                    <div class="point">
                                        <div class="logo">
                                            <i class="fas fa-sync-alt"></i>
                                        </div>
                                        <div class="item">
                                            <div class="box">
                                                <?php the_field('savety_sop'); ?>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="box">
                                                <?php the_field('savety_shav'); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End of data 2 -->

                                    <!-- Data item 3 -->
                                    <div class="point">
                                        <div class="logo">
                                        <i class="fas fa-euro-sign"></i>
                                        </div>
                                        <div class="item">
                                            <div class="box">
                                                <?php the_field('money_sop'); ?>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="box">
                                                <?php the_field('money_shav'); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End of data 3 -->

                                    <!-- Data item 4 -->
                                    <div class="point">
                                        <div class="logo">
                                            <!-- Here is the icon -->
                                            <i class="fas fa-heart"></i>
                                        </div>
                                        <div class="item">
                                            <div class="box">
                                                <?php the_field('growth_sop'); ?>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="box">
                                                <?php the_field('growth_shav'); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End of data 4 -->

                                    <!-- Data item 5 -->
                                    <div class="point">
                                        <div class="logo">
                                            <!-- Here is the icon -->
                                            <i class="fas fa-bell-slash"></i>
                                        </div>
                                        <div class="item">
                                            <div class="box">
                                                <?php the_field('additional_sop'); ?>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="box">
                                                <?php the_field('additional_shav'); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End of data 5 -->
                                </div>
                                <div class="bottom">
                                    <div><?php _e('En nog veel meer...', 'nakedlaser'); ?></div>
                                    <div class="button-wrapper">
                                        <a href="<?php the_field('refers_page_service', 'option'); ?>" target="_blank" class="btn black"><?php _e('Reserveren', 'nakedlaser'); ?></a>
                                        <div class="hide"><?php _e('OF', 'nakedlaser'); ?></div>
                                        <a href="<?php the_field('contactus_button'); ?>" class="btn black-2"><?php _e('Contact Ons','nakedlaser'); ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif ;?>
                
                <?php if(get_field('title_gallery_lsr') && get_field('pick_image')) : ?>
                    <div class="section section-full-gallery">
                        <div class="container">
                            <div class="col-12 col-md-8 col-lg-6 mx-auto">
                                <div class="content-set">
                                    <h3 class="title">
                                        <?php the_field('title_gallery_lsr'); ?>
                                    </h3>
                                    <div class="content">
                                        <?php the_field('subtitle_gallery_lsr'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php
                            $image_ids = get_field('pick_image', false, false);
                            $shortcode = '[' . 'gallery ids="' . implode(',', $image_ids) . '"]';

                            echo do_shortcode($shortcode);
                        ?>
                    </div>
                <?php endif ; ?>
                
                <?php get_template_part('template-parts/component/call-to-action-reserveren'); ?>

            </div>
        </div>

<?php 
    endwhile;
endif;

get_footer();