<?php
/**
 * Template part for displaying Price list
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * Template Name: Tarieven Page
 * @package NakedLaser
 */

 get_header();
 
 if(have_posts()) :
    while (have_posts()) : the_post(); ?>

        <div id="content">
            <div id="tarieven">
                
                <div class="section full-thumbnail">
                    <div class="bg-image" <?php if( has_post_thumbnail() ){ 
                            echo 'style="background-image: url(\'' . get_the_post_thumbnail_url() . '\')"';
                        } ?>></div>    
                    <svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="1366px" height="81px" version="1.1" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd"
                        viewBox="0 0 584 35"
                        xmlns:xlink="http://www.w3.org/1999/xlink"
                        class="the-wave the-wave-1"
                        preserveAspectRatio="none">
                        <defs>
                        </defs>
                        <g id="Layer_x0020_1">
                        <metadata id="CorelCorpID_0Corel-Layer"/>
                        <path class="the-wave-path" d="M584 13l0 22 -584 0c0,-39 0,17 0,-22 95,-17 192,-17 292,0 98,19 195,19 292,0z"/>
                        <rect x="0" y="40" width="100%" height="100" />
                        </g>
                    </svg>

                    <div class="container container-content">
                        <div class="content-set">
                            <h1 class="title">
                                <?php the_title(); ?>
                            </h1>
                            <div class="content theme-wp-content">
                                <?php the_field('subtitle_hdr_service'); ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="section section-the-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-md-8 mx-auto" id="the-content">
                                <?php the_content(); ?>
                            </div>
                        </div>
                    </div>
                </div>
                
                <?php if(get_field('title_tarieven') && get_field('content_tarieven')) : ?>
                    <div class="section section-price-list">
                        <div class="container">
                        
                            <div class="col-12 col-md-8 col-lg-6 mx-auto">
                                <div class="content-set">
                                    <h3 class="title">
                                        <?php the_field('title_tarieven'); ?>
                                    </h3>
                                    <div class="content">
                                        <?php the_field('content_tarieven'); ?>
                                    </div>
                                </div>
                            </div>
                            
                            <?php if(get_field('tarieven_breakdown_woman') && get_field('tarieven_breakdown_man')) : ?>
                                <div class="row">
                                    <div class="col-12 col-md-6">
                                        <div class="item">
                                            <div class="header">
                                                <?php the_field('title_woman'); ?>
                                            </div>
                                            <div class="price-list">
                                                <!-- Here is the data -->
                                                <?php if(have_rows('tarieven_breakdown_woman')): 
                                                    while(have_rows('tarieven_breakdown_woman')) : the_row(); ?>
                                                    <div class="price-item">
                                                        <div class="name">
                                                            <?php the_sub_field('category_woman'); ?>
                                                        </div>
                                                        <div class="one price">
                                                            <span><?php _e('Losse prijs', 'nakedlaser'); ?></span>
                                                            <span><?php the_sub_field('loose_woman'); ?></span>
                                                        </div>
                                                        <div class="group price">
                                                            <span><?php _e('Pakket prijs', 'nakedlaser'); ?></span>
                                                            <span><?php the_sub_field('pakket_woman'); ?></span>
                                                        </div>
                                                    </div>
                                                    <?php endwhile ;
                                                endif ; ?>
                                                <!-- End of the data -->

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <div class="item">
                                            <div class="header">
                                                <?php the_field('title_man'); ?>
                                            </div>
                                            <div class="price-list">
                                                <!-- Here is the data -->
                                                <?php if(have_rows('tarieven_breakdown_man')): 
                                                    while(have_rows('tarieven_breakdown_man')) : the_row(); ?>
                                                        <div class="price-item">
                                                            <div class="name">
                                                                <?php the_sub_field('category_man'); ?>
                                                            </div>
                                                            <div class="one price">
                                                                <span><?php _e('Losse prijs', 'nakedlaser'); ?></span>
                                                                <span><?php the_sub_field('loose_man'); ?></span>
                                                            </div>
                                                            <div class="group price">
                                                                <span><?php _e('Pakket prijs', 'nakedlaser'); ?></span>
                                                                <span><?php the_sub_field('pakket_man'); ?></span>
                                                            </div>
                                                        </div>
                                                    <?php endwhile ;
                                                endif ; ?>
                                                <!-- End of the data -->

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <div class="item">
                                            <div class="header">
                                                <?php the_field('title_additional_1'); ?>
                                            </div>
                                            <div class="price-list">
                                                <!-- Here is the data -->
                                                <?php if(have_rows('tarieven_breakdown_additional_1')): 
                                                    while(have_rows('tarieven_breakdown_additional_1')) : the_row(); ?>
                                                        <div class="price-item">
                                                            <div class="name">
                                                                <?php the_sub_field('category_man'); ?>
                                                            </div>
                                                            <div class="one price">
                                                                <span><?php _e('Losse prijs', 'nakedlaser'); ?></span>
                                                                <span><?php the_sub_field('loose_man'); ?></span>
                                                            </div>
                                                            <div class="group price">
                                                                <span><?php _e('Pakket prijs', 'nakedlaser'); ?></span>
                                                                <span><?php the_sub_field('pakket_man'); ?></span>
                                                            </div>
                                                        </div>
                                                    <?php endwhile ;
                                                endif ; ?>
                                                <!-- End of the data -->

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <div class="item">
                                            <div class="header">
                                                <?php the_field('title_additional_2'); ?>
                                            </div>
                                            <div class="price-list">
                                                <!-- Here is the data -->
                                                <?php if(have_rows('tarieven_breakdown_additional_2')): 
                                                    while(have_rows('tarieven_breakdown_additional_2')) : the_row(); ?>
                                                        <div class="price-item">
                                                            <div class="name">
                                                                <?php the_sub_field('category_man'); ?>
                                                            </div>
                                                            <div class="one price">
                                                                <span><?php _e('Losse prijs', 'nakedlaser'); ?></span>
                                                                <span><?php the_sub_field('loose_man'); ?></span>
                                                            </div>
                                                            <div class="group price">
                                                                <span><?php _e('Pakket prijs', 'nakedlaser'); ?></span>
                                                                <span><?php the_sub_field('pakket_man'); ?></span>
                                                            </div>
                                                        </div>
                                                    <?php endwhile ;
                                                endif ; ?>
                                                <!-- End of the data -->

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif ; ?>
                        </div>
                    </div>
                <?php endif ; ?>
                
                <?php get_template_part('template-parts/component/call-to-action-reserveren'); ?>

            </div>
        </div>

<?php 
    endwhile;
endif;

get_footer();