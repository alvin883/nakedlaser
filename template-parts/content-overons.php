<?php
/**
 * Template part for displaying Overons Page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * Template Name: Overons Page
 * @package NakedLaser
 */

 get_header();
 
 if(have_posts()) :
    while (have_posts()) : the_post(); ?>

        <div id="content">
            <div id="overons">
                
                <div class="section full-thumbnail">
                    <div class="bg-image" <?php if( has_post_thumbnail() ){ 
                            echo 'style="background-image: url(\'' . get_the_post_thumbnail_url() . '\')"';
                        } ?>></div>
                    <svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="1366px" height="81px" version="1.1" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd"
                        viewBox="0 0 584 35"
                        xmlns:xlink="http://www.w3.org/1999/xlink"
                        class="the-wave the-wave-1"
                        preserveAspectRatio="none">
                        <defs>
                        </defs>
                        <g id="Layer_x0020_1">
                        <metadata id="CorelCorpID_0Corel-Layer"/>
                        <path class="the-wave-path" d="M584 13l0 22 -584 0c0,-39 0,17 0,-22 95,-17 192,-17 292,0 98,19 195,19 292,0z"/>
                        <rect x="0" y="40" width="100%" height="100" />
                        </g>
                    </svg>

                    <div class="container container-content">
                        <div class="col-12 col-md-8 col-lg-6 mx-auto">
                            <div class="content-set">
                                <h1 class="title">
                                    <?php the_title(); ?>
                                </h1>
                                <div class="content theme-wp-content">
                                    <?php the_field('subtitle_header'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="section section-the-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-md-8 mx-auto" id="the-content">
                                <?php the_content(); ?>
                            </div>
                        </div>
                    </div>
                </div>

                <?php if(get_field('title_ts') && get_field('team_member')) : ?>
                    <div class="section section-team">
                        <div class="container">
                            <div class="col-12 col-md-8 col-lg-6 mx-auto">
                                <div class="content-set">
                                    <h3 class="title">
                                        <?php the_field('title_ts'); ?>
                                    </h3>
                                    <div class="content">
                                        <?php the_field('subtitle_ts'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <?php if(have_rows('team_member')) : 
                                    while(have_rows('team_member')) : the_row(); 
                                        $image = get_sub_field('photos_profil'); ?>
                                        <div class="column col-12 col-sm-6 col-lg-4 mx-auto">
                                            <div class="item">
                                                <div class="photo" 
                                                    style="background-image: url('<?php echo $image['url']; ?>');"></div>
                                                <div class="name">
                                                    <?php the_sub_field('name_team'); ?>
                                                </div>
                                                <div class="description">
                                                    <?php the_sub_field('description_ts'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endwhile ; 
                                endif ; ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                
                <?php
                    $images = get_field('gallery_overons') ; 
                    if($images) : ?>
                        <div class="section section-full-gallery">
                            <div class="container">
                                <div class="col-12 col-md-8 col-lg-6 mx-auto">
                                    <div class="content-set">
                                        <h3 class="title">
                                        <?php 
                                            the_field('gallery_overons_titel');
                                        ?>
                                        </h3>
                                        <div class="content">
                                        <?php 
                                            the_field('gallery_overons_omschrijving');
                                        ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                                $image_ids = get_field('gallery_overons', false, false);
                                $shortcode = '[' . 'gallery ids="' . implode(',', $image_ids) . '"]'; 
                                echo do_shortcode($shortcode);
                            ?>
                        </div>
                <?php endif ; ?>
                
            </div>
        </div>

<?php 
    endwhile;
endif;

get_footer();