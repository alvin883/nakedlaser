<?php
/**
 * Template part for displaying list of all Services Page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * Template Name: Services Page
 * @package NakedLaser
 */

 get_header();
 
 if(have_posts()) :
    while (have_posts()) : the_post(); ?>

        <div id="content">
            <div id="services">
                
                <div class="section full-thumbnail">
                    <div class="bg-image" <?php if( has_post_thumbnail() ){ 
                            echo 'style="background-image: url(\'' . get_the_post_thumbnail_url() . '\')"';
                        } ?>></div>    
                    <svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="1366px" height="81px" version="1.1" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd"
                        viewBox="0 0 584 35"
                        xmlns:xlink="http://www.w3.org/1999/xlink"
                        class="the-wave the-wave-1"
                        preserveAspectRatio="none">
                        <defs>
                        </defs>
                        <g id="Layer_x0020_1">
                        <metadata id="CorelCorpID_0Corel-Layer"/>
                        <path class="the-wave-path" d="M584 13l0 22 -584 0c0,-39 0,17 0,-22 95,-17 192,-17 292,0 98,19 195,19 292,0z"/>
                        <rect x="0" y="40" width="100%" height="100" />
                        </g>
                    </svg>

                    <div class="container container-content">
                        <div class="col-12 col-md-8 col-lg-6 mx-auto">
                            <div class="content-set">
                                <h1 class="title">
                                    <?php the_title(); ?>
                                </h1>
                                <div class="content theme-wp-content">
                                    <?php the_field('subtitle_hdr_services'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="section section-the-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-md-8 mx-auto" id="the-content">
                                <?php the_content(); ?>
                            </div>
                        </div>
                    </div>
                </div>
                
                <?php if(get_field('title_services') && get_field('content_services')) : ?>
                    <div class="section section-card">
                        <div class="container">
                            <div class="col-12 col-md-8 col-lg-6 mx-auto">
                                <div class="content-set">
                                    <h3 class="title">
                                        <?php the_field('title_services'); ?>
                                    </h3>
                                    <div class="content">
                                        <?php the_field('content_services'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <?php 
                                global $post;
                                $child = new WP_Query(array(
                                    'post_parent' => $post->ID,
                                    'post_type' => 'page',
                                    'post_per_page' => -1,
                                )); 
                                if($child->have_posts()) : 
                                    while($child->have_posts()) : $child->the_post();
                                ?>
                                        <div class="column col-12 col-sm-6 col-lg-4">
                                            <a href="<?php the_permalink(); ?>">
                                                <div class="item">
                                                    <div class="photo"<?php if(has_post_thumbnail()) {
                                                        echo 'style="background-image: url(\'' . get_the_post_thumbnail_url() . '\')"'; } ?>></div>
                                                        <div class="content-set">
                                                            <div class="title">
                                                                <?php the_title(); ?>
                                                            </div>
                                                        </div>
                                                </div>
                                            </a>
                                        </div>
                                    <?php endwhile ; 
                                endif ; ?>
                            </div>
                        </div>
                    </div>
                <?php endif ; ?>
                
                <?php get_template_part('template-parts/component/call-to-action-reserveren'); ?>

            </div>
        </div>

<?php 
    endwhile;
endif;

get_footer();