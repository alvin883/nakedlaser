<?php
/**
 * Template part for displaying Contact Page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * Template Name: Contact Page
 * @package NakedLaser
 */

 get_header();
 
 if(have_posts()) :
    while (have_posts()) : the_post(); ?>

        <div id="content">
            <div id="contact">
                
                <div class="section full-thumbnail">
                    <div class="bg-image" <?php if( has_post_thumbnail() ){ 
                            echo 'style="background-image: url(\'' . get_the_post_thumbnail_url() . '\')"';
                        } ?>></div>    
                    <svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="1366px" height="81px" version="1.1" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd"
                        viewBox="0 0 584 35"
                        xmlns:xlink="http://www.w3.org/1999/xlink"
                        class="the-wave the-wave-1"
                        preserveAspectRatio="none">
                        <defs>
                        </defs>
                        <g id="Layer_x0020_1">
                        <metadata id="CorelCorpID_0Corel-Layer"/>
                        <path class="the-wave-path" d="M584 13l0 22 -584 0c0,-39 0,17 0,-22 95,-17 192,-17 292,0 98,19 195,19 292,0z"/>
                        <rect x="0" y="40" width="100%" height="100" />
                        </g>
                    </svg>

                    <div class="container container-content">
                        <div class="col-12 col-md-8 col-lg-6 mx-auto">
                            <div class="content-set">
                                <h1 class="title">
                                    <?php the_title(); ?>
                                </h1>
                                <div class="content theme-wp-content">
                                    <?php the_field('subtitle_contact'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="section section-the-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-md-8 mx-auto" id="the-content">
                                <?php the_content(); ?>
                                <div>
                                    <?php echo do_shortcode(get_field('contact_shortcode')); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <?php if(get_field('title_contact_lgt') && get_field('title_contact_schedule')) : ?>
                    <div class="section section-contact">
                        <div class="container">
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="item">
                                        <h3 class="header">
                                            <?php the_field('title_contact_lgt'); ?>
                                        </h3>
                                        <div class="content-table">
                                            <div>
                                                <div>
                                                    <?php _e('Email', 'nakedlaser'); ?>
                                                </div>
                                                <div>:
                                                    <a href="mailto:blablabl@gmail.com"><?php the_field('email', 'option'); ?></a>
                                                </div>
                                            </div>
                                            <div>
                                                <div><?php _e('Phone', 'nakedlaser'); ?></div>
                                                <div>: 
                                                    <a href="tel:+0812908123"><?php the_field('telp', 'option'); ?></a>
                                                </div>
                                            </div>
                                            <?php if(get_field('kvk') && get_field('btw')) : ?>
                                                <div>
                                                    <div><?php _e('KvK', 'nakedlaser'); ?></div>
                                                    <div>: <?php the_field('kvk'); ?></div>
                                                </div>
                                                <div>
                                                    <div><?php _e('BTW', 'nakedlaser'); ?></div>
                                                    <div>: <?php the_field('btw'); ?></div>
                                                </div>
                                            <?php endif ; ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="item">
                                        <h3 class="header">
                                            <?php the_field('title_contact_schedule') ?>
                                        </h3>
                                        <div class="content-table schedule">
                                        <?php if (have_rows('daily_time', 'option')) :
                                            while (have_rows('daily_time', 'option')) : the_row(); ?>
                                                <div>
                                                    <div> <?php echo "<span>" . get_sub_field('open_daily', 'option') . "</span>"; ?></div>
                                                </div>
                                        
                                            <?php endwhile; 
                                        endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif ; ?>
                
                <?php if(get_field('title_maps') && get_field('gmaps')) : ?>
                    <div class="section full-gmaps">
                        <div class="container">
                            <div class="col-12 col-md-8 col-lg-6 mx-auto">
                                <div class="content-set centered">
                                    <h2 class="title">
                                        <?php the_field('title_maps'); ?>
                                    </h2>
                                    <div class="content">
                                        <?php the_field('subtitle2_maps'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wrapper">
                            <?php the_field('gmaps'); ?>
                        </div>
                    </div>
                <?php endif ; ?>

            </div>
        </div>

<?php 
    endwhile;
endif;

get_footer();