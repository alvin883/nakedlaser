<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package leenderhof
 */

get_header(); ?>

    <div id="content">

            <div class="section full-thumbnail">
                <div class="bg-image" <?php if( has_post_thumbnail() ){ 
                        echo 'style="background-image: url(\'' . get_the_post_thumbnail_url() . '\')"';
                    } ?>></div>
                <svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="1366px" height="81px" version="1.1" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd"
                    viewBox="0 0 584 35"
                    xmlns:xlink="http://www.w3.org/1999/xlink"
                    class="the-wave the-wave-1"
                    preserveAspectRatio="none">
                    <defs>
                    </defs>
                    <g id="Layer_x0020_1">
                    <metadata id="CorelCorpID_0Corel-Layer"/>
                    <path class="the-wave-path" d="M584 13l0 22 -584 0c0,-39 0,17 0,-22 95,-17 192,-17 292,0 98,19 195,19 292,0z"/>
                    <rect x="0" y="40" width="100%" height="100" />
                    </g>
                </svg>
                <div class="container container-content">
                    <div class="content-set archive">

                        
                        <div class="subtitle">
                            <?php _e('Archive page', 'leenderhof'); ?>
                        </div>

                        <?php if(is_year()) { ?>
                            <h1 class="title">
                                <?php _e('Year ', 'leenderhof'); the_time('Y'); ?>
                            </h1> 
                        <?php } else if(is_month()) { ?>
                            <h1 class="title">
                                <?php the_time('F Y'); ?>
                            </h1>
                        <?php } else if(is_date()) { ?>
                            <h1 class="title">
                                <?php the_time('d F Y'); ?>
                            </h1>
                        <?php } ?>

                        <div class="subtitle">
                            <?php echo 'Found ' . $GLOBALS['wp_query']->post_count . ' post'; ?>
                        </div>

                    </div>
                </div>
            </div>

            <div class="section section-card white">
                <div class="container">
                    <div class="row">
                        <?php 
                        if(have_posts()) : 
                            while(have_posts()) : the_post();
                        ?>
                            <div class="column col-12 col-sm-6 col-lg-4">
                                <a href="<?php the_permalink(); ?>">
                                    <div class="item">
                                        <div class="photo"<?php if(has_post_thumbnail()) {
                                            echo 'style="background-image: url(\'' . get_the_post_thumbnail_url() . '\')"'; } ?>></div>
                                        <div class="content-set">
                                            <h4 class="title">
                                                <?php the_title(); ?>
                                            </h4>
                                            <div class="subtitle">
                                                <?php the_time('F j, Y'); ?>
                                            </div>
                                            <div class="content">
                                                <?php echo content(30); ?>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <?php endwhile ; 
                        endif ; ?>
                    </div>
                </div>
            </div>

            <!-- Bigger than 500 px screen -->
            <div class="post-navigation wide">
                <div class="info">
                    <?php 
                        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                        echo "Page " . $paged . " of " . $wp_query->max_num_pages;
                    ?>
                </div>
                <div class="all-page-number">
                    <?php 
                        echo paginate_links(array(
                            'total' => $wp_query->max_num_pages
                        ));
                    ?>
                </div>
            </div>

            <!-- Smaller than 500 px screen -->
            <div class="post-navigation mobile">
                <div class="info">
                    <?php 
                        echo "Page " . $paged . " of " . $wp_query->max_num_pages;
                    ?>
                </div>
                <div class="all-page-number">
                    <?php
                        previous_posts_link('&laquo; Previous');
                        next_posts_link('Next &raquo;'); 
                    ?>
                </div>
            </div>
            
    </div>

<?php get_footer(); ?>
