<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package jcm
 */

get_header(); ?>

	<div id="content">
        <div id="page-404">
			<div class="section section-1">
				<div class="container">
					<div class="content-set">
						<iframe src="https://giphy.com/embed/xT9KVGJrVozz6sYjGE" width="480" height="258" frameBorder="0" class="giphy-embed" allowFullScreen></iframe>
						<h1 class="title">Oopps, 404 Error !</h1>
						<div class="content">
							Sorry, we can't find the page you are looking for, you entering empty state .
							<a href="<?php echo get_home_url(); ?>" class="btn">
								Go to Homepage
							</a>
						</div>
					</div>
				</div>
			</div>
        </div><!-- #page-404 -->
	</div><!-- #content-->
	
<?php
get_footer();
