<?php
/**
 * @package jcm
 * @subpackage theme name here
 * 	
 */

get_header(); ?>

<div id="homepage">

    <?php 
        get_template_part('template-parts/homepage/section-1(hero)');
        get_template_part('template-parts/homepage/section-2(overons)');
        get_template_part('template-parts/homepage/section-3(service)');
        get_template_part('template-parts/homepage/section-4(timeline)');
        get_template_part('template-parts/homepage/section-5(video)');
        get_template_part('template-parts/homepage/section-6(points)');
    ?>

 </div>

<?php 
get_footer();
