<?php

/**
 * Enqueue scripts and styles.
 */
function jcm_scripts() {

	// The Javascript
	wp_enqueue_script( 'jcm-js', get_template_directory_uri() . '/js/dist/scripts.min.js', array('jquery'), false, true );

	// The CSS
	wp_enqueue_style( 'jcm-style', get_stylesheet_directory_uri() . '/style.min.css', array(), '1.0.0' );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'jcm_scripts' );


/**
 * Filter the HTML script tag of `leadgenwp-fa` script to add `defer` attribute.
 *
*/
// function jcm_defer_scripts( $tag, $handle, $src ) {
// 	// The handles of the enqueued scripts we want to defer
// 	$defer_scripts = array( 
// 		'jcm-fa'
// 	);
//     if ( in_array( $handle, $defer_scripts ) ) {
//         return '<script src="' . $src . '" defer></script>';
//     }
//     return $tag;
// }
// add_filter( 'script_loader_tag', 'jcm_defer_scripts', 10, 3 );