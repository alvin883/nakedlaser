<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package NakedLaser
 */

?>
<?php if(!is_blog()) :?>
	<?php if(get_field('title_nieuwsbrief', 'option')) : ?>
		<div class="section section-newsletter">
			<div class="container">
				<div class="row">
					<div class="column col-md-12 col-lg-8 mx-auto">
						<div class="box">
							<div class="content-set">
								<h3 class="title">
									<?php the_field('title_nieuwsbrief', 'option'); ?>
								</h3>
								<div class="content">
									<?php the_field('content_nieuwsbrief', 'option'); ?>
								</div>
							</div>

							<!-- Form -->
							<div class="form">
								<div class="input">
									<?php
										echo do_shortcode(get_field('nieuwsbrief_code', 'option'));
									?>
								</div>
							</div><!-- .form -->
						</div><!-- .box -->
					</div><!-- .col -->
				</div>
			</div>
		</div>
	<?php endif; ?>
<?php endif; ?>

<footer id="footer"<?php echo (is_blog()) ? ' class="grey"' : ''; ?>>

	<div class="wave wave-1">
		<svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="1366px" height="81px" version="1.1" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd"
            viewBox="0 0 584 35"
            xmlns:xlink="http://www.w3.org/1999/xlink"
            class="the-wave the-wave-1"
            preserveAspectRatio="none">
            <defs>
            </defs>
            <g id="Layer_x0020_1">
            <metadata id="CorelCorpID_0Corel-Layer"/>
			<path class="the-wave-path" d="M584 13l0 22 -584 0c0,-39 0,17 0,-22 95,-17 192,-17 292,0 98,19 195,19 292,0z"/>
			<rect x="0" y="40" width="100%" height="100" />
            </g>
        </svg>
		<svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="1366px" height="81px" version="1.1" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd"
            viewBox="0 0 584 35"
            xmlns:xlink="http://www.w3.org/1999/xlink"
            class="the-wave the-wave-2"
            preserveAspectRatio="none">
            <defs>
            </defs>
            <g id="Layer_x0020_1">
            <metadata id="CorelCorpID_0Corel-Layer"/>
            <path class="the-wave-path" d="M584 13l0 22 -584 0c0,-39 0,17 0,-22 95,-17 192,-17 292,0 98,19 195,19 292,0z"/>
            </g>
        </svg>
	</div>

	<div class="top">
		<div class="container">
			<div class="row">
				<div class="col-12 col-sm-6 col-lg-3 paginas">
					<div class="title">
						<?php _e('Pagina\'s', 'nakedlaser'); ?>
					</div>
					<div class="content">
						<?php if(has_nav_menu('footer')) { wp_nav_menu('footer'); } ?>
					</div>
				</div> 
				<div class="col-12 col-sm-6 col-lg-3 diesten">
					<div class="title">
						<?php _e('Diensten', 'nakedlaser'); ?>
					</div>
					<div class="content">
						<?php $post_object = get_field('parent_page', 'option');
						if($post_object) : 
							$child = new WP_Query(array(
								'post_parent' => $post_object->ID,
								'post_type' => 'page',
							)); 
							if ($child->have_posts()) :
								while ($child->have_posts()) : $child->the_post(); ?>
									<ul>
										<li>
											<a href="<?php the_permalink(); ?>">
												<?php the_title(); ?>
											</a>
										</li>
									</ul>
								<?php endwhile ; 
							endif ; ?> 
						<?php endif ; ?>
					</div>
					
				</div> 
				<div class="col-12 col-sm-6 col-lg-3 contact">
					<div class="title">
						<?php _e('Contactgegevens', 'nakedlaser'); ?>
					</div>
					<div class="content">
						<div class="item">
							<?php the_field('adres', 'option'); ?>
						</div>
						<div class="item">
							<a href="tel:<?php echo str_replace(' ', '', str_replace('-', '', get_field('telp', 'option'))); ?>">
								<?php the_field('telp', 'option'); ?>
							</a>
						</div>
						<div class="item">
							<a href="mailto:<?php the_field('email', 'option'); ?>">
								<?php the_field('email', 'option'); ?>
							</a>
						</div>
					</div>
				</div>               
				<div class="col-12 col-sm-6 col-lg-3 opening">
					<div class="title">
						<?php _e('Openingstijden', 'nakedlaser'); ?>
					</div>
					<div class="content">
						<?php if (have_rows('daily_time', 'option')) :
							while (have_rows('daily_time', 'option')) : the_row(); ?>
								<div class="item">
									<?php
										the_sub_field('days', 'option');
										echo "<span>" . get_sub_field('open_daily', 'option') . "</span>";
									?>
								</div>
							<?php endwhile; 
						endif; ?>
					</div>
				</div>
			</div><!-- .row -->
		</div>
	</div>

	<div class="bottom">
		<button class="btn jump-to-top circle white"><i class="fas fa-chevron-right icon"></i></button>
		<svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="1366px" height="81px" version="1.1" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd"
            viewBox="0 0 584 35"
            xmlns:xlink="http://www.w3.org/1999/xlink"
            class="the-wave the-wave-1"
            preserveAspectRatio="none">
            <defs>
            </defs>
            <g id="Layer_x0020_1">
            <metadata id="CorelCorpID_0Corel-Layer"/>
            <path class="the-wave-path" d="M584 13l0 22 -584 0c0,-39 0,17 0,-22 95,-17 192,-17 292,0 98,19 195,19 292,0z"/>
            </g>
		</svg>
		<?php if(get_field('facebook', 'option') && get_field('instagram', 'option')) : ?>
			<div class="container socmed">
				<div class="wrapper">
					<a href="<?php the_field('facebook', 'option'); ?>" target= "_blank" class="btn">
						<i class="fab fa-facebook-f"></i>
					</a>
					<a href="<?php the_field('instagram', 'option'); ?>" target="_blank" class="btn">
						<i class="fab fa-instagram"></i>
					</a>
				</div>
			</div>
		<?php endif ; ?>
		<div class="container copyright">
			<div class="left">
				&copy; Copyright 2019 - <b>NAKED Laser Clinic</b>
			</div>
			<div class="right">
				Designed & Developed by 
				<a href="https://www.wappstars.nl/" target="_blank"><b>WappStars</b></a>
			</div>
		</div>
	</div>
		
</footer>
<?php 
	wp_footer();
?>
</body>
</html>

