<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package leenderhof
 */

get_header(); ?>

<div id="overons">
	<div id="content">
		<div class="section section-2">
			<div class="container">
				<div class="row">
					<div class="col-12 col-lg-8">
						<div id="the-content">
							<?php if ( have_posts() ) :
								while ( have_posts() ) : the_post(); ?>

										<div class="post-thumbnail">
											<a href="<?php the_permalink(); ?>">
												<div class="photo" <?php if( has_post_thumbnail() ) :
													echo 'style="background-image: url(\'' . get_the_post_thumbnail_url() . '\');"';
												endif;
												?>></div>
											</a>
											<div class="post-content">
												<a href="<?php the_permalink(); ?>">
													<h3>
														<?php the_title();?>
													</h3>
												</a>
												<h6>
													<?php the_time('F j, Y'); ?>
												</h6>
												<div id="the_content">
													<?php echo content(40); ?>
												</div>
												<a href="<?php the_permalink(); ?>" class="btn">Read More</a>
											</div>
										</div>

							<?php endwhile;
								echo paginate_links();
							else :
								get_template_part( 'template-parts/content', 'none' );
							endif; ?>
						</div>
					</div>	

				</div>
			</div>
		</div>
	</div>
</div>

			
<?php
	get_footer();
