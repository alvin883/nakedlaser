jQuery(document).ready(function($){

    var nav         = ".the-navbar",
        navButton   = nav + " .nav-button",
        navClose    = nav + " .nav-close",
        navBox      = nav + " .nav-box",
        navBlock    = nav + " .blocking";

    // Initialize the navbar, disable all the tabindex
    $(navBox).find('a').attr('tabindex','-1');
    $(navClose).attr('tabindex','-1');

    // Activate the '.nav-box'
    $(navButton).on("click hitEnter",function(e){
        // if user click 'nav-close', don't fire this event
        if($(e.target).hasClass('nav-close') || $(e.target).parents().hasClass('nav-close')) return false;
        // if its has been active, don't fire this event
        if( !$(this).hasClass('active') ) {
            var goal = $(navBox);
            $(this).addClass('active')
                .attr('tabindex',-1);
            goal.find('a').attr('tabindex',0);
            $(navClose).attr('tabindex',0);
            $(navBlock).addClass("active");
            resizeNavButton(true);
        }
    }).on('keypress', function(e){
        if(e.which === 13 ) $(this).trigger("hitEnter");
    });

    // Deactivate the '.nav-box'
    $( navBlock + "," + navClose ).click(function(e){
        $( navButton+".active").removeClass("active")
            .css({
                "height"    : "40px",
                "width"     : "40px"
            });
        // Switch Tabindex
        $(navButton).attr('tabindex',0);
        $(navBox).find('a').attr('tabindex',-1);
        $(navClose).attr('tabindex',-1);
        // Remove active style
        $( navBlock ).removeClass("active");
        // Force hide bootstrap dropdown
        $(".navbar").trigger('hidden.bs.dropdown');

        $(nav + ' .navbar-nav').find('li a.sub-active').each(function(){
            $(this).removeClass('sub-active').parent().removeClass('sub-active')
                .find('.sub-menu-wrapper').css({'height': 0});
        });
    });

    // Resize nav-button
    function resizeNavButton(force = false, plusSize = 0, actualGoalHeight = null){
        // Only resize if 'nav-button' active
        if( !force && !$( navButton ).hasClass("active") ) return false;
        
        var limitHeight = parseFloat( $(navBox).css('max-height') ),
            goal        = $(navBox),
            goalHeight  = ( goal[0].offsetHeight + plusSize > limitHeight ) ?
                                limitHeight :
                                goal[0].offsetHeight + plusSize;
        if( actualGoalHeight ) goalHeight = ( actualGoalHeight > limitHeight ) ?
                                limitHeight :
                                actualGoalHeight;
        $( navButton ).addClass('active').css({
            "height"    : goalHeight,
            "width"     : goal[0].offsetWidth
        });
    }

    // Child trigger
    $('li.menu-item-has-children > a').click(function(){
        if($(this).hasClass('sub-active')){
            var li          = $($(this).parent()),
                goalHeight  = $(navBox)[0].scrollHeight - li.find('.sub-menu-wrapper').height();

            $(this).removeClass('sub-active');
            li.removeClass('sub-active');
            li.find('.sub-menu-wrapper').css({'height': 0 + 'px'});
            resizeNavButton(false, 0, goalHeight);
        } else {
            var li          = $($(this).parent()),
                goalHeight  = li.find('.sub-menu-wrapper .sub-menu').height() + 20;
            
            $(this).addClass('sub-active');
            li.addClass('sub-active');
            li.find('.sub-menu-wrapper').css({'height': goalHeight + 'px'});
            resizeNavButton(false, goalHeight);
        }
        return false;
    });

    

    // Target URL (#blablabla) onload
    if( $(window.location.hash).length ){
        $('html,body').animate({
            scrollTop: $(window.location.hash).offset().top
        }, 500, 'swing');
    }
    
    // Target URL (#blablabla) onclick *In same location
    $('a[href^="#"]').click(function(e){
        e.preventDefault();
        $('html,body').animate({
            scrollTop: $($(this).attr('href')).offset().top
        }, 500, 'swing');
        if( $(this).parents(".navbar-nav") && $(this).parents("nav") ){
            $($(this).parents()[1]).find("li.active").each(function(){ // Find current .active element
                $(this).removeClass("active");
            });
            $(this).parent().addClass("active");
        }
    });
    
    // Navbar, Toggle small navigation when user scroll
    function checkNavbar(){
        var scroll = $(window).scrollTop();
        if(scroll > 0){
            $("#the-navbar").addClass("white");
        }else{
            $("#the-navbar").removeClass("white");
        }
    }
    // Check onReady
    checkNavbar();
    // Check onScroll
    $(window).scroll(function(){
        checkNavbar();
    });

    $.fn.scrollAnimation = function(){
        var el = $(this),
            matrix = el.css('transform').replace(/[^0-9\-.,]/g, '').split(','),
            transY = matrix[13] || matrix[5],
            // Remove the translate value, cause we don't need that
            expectedY = el.offset().top - transY;

        if( $(window).scrollTop() + $(window).height() - 150 > expectedY ){
            el.addClass("visible");
            return true;
        } else {
            el.removeClass("visible");
        }
        return false;
    }

    /** Swipebox */
    $( '.gallery-native,.wp-block-gallery' ).each(function(i){
        var newClass= 'gallery_'+ i;
        $(this).addClass(newClass);
        $('.' + newClass + ' a').swipebox();
    });

    if($("#homepage").length){
        /** Hero Slider */
        var heroSlider = '.section-1 .container .left .slider',
        heroSLiderButton = '.section-1 .container .left .dots button';
        $(heroSlider).on('init', function(slick){
            $('.section-1').addClass('slider-loaded');
        }).slick({
            dots: false,
            arrows: false,
            autoplay: false,
            fade: true
        });

        $(heroSlider).on('beforeChange', function(event, slick, currentSlide, nextSlide){
            $(heroSLiderButton+".active").removeClass("active");
            $(heroSLiderButton+".dot-index-"+nextSlide).addClass("active");
        });

        $(heroSLiderButton).click(function(){
            if($(this).hasClass("active")) return false;
            var destination = $(this).attr("class").split("-")[2];
            $(heroSlider).slick("slickGoTo", destination);
        });

        /** Service Slider */
        var serviceSliderContent = '.section-3 .column-content .slider-content-wrapper',
            serviceSliderButton = '.section-3 .column-content .wrapper-button .slider-button-wrapper',
            serviceSliderPhoto = '.section-3 .slider-photo-wrapper',
            serviceSLiderController = '.section-3 .column-content .wrapper-button .btn';
            
        $(serviceSliderContent + ',' + serviceSliderButton + ',' + serviceSliderPhoto)
        .on('init', function(slick){
            $('.section-3').addClass('slider-loaded');
        }).slick({
            dots: false,
            arrows: false,
            autoplay: false,
            fade: true,
            asNavFor: '.service-slider'
        });

        $(serviceSLiderController+'.next').click(function(){
            $(serviceSliderContent).slick('slickNext');
        });

        $(serviceSLiderController+'.prev').click(function(){
            $(serviceSliderContent).slick('slickPrev');
        });

        /** Homepage Video */
        $('.section-5 .play-video').click(function(){
            $('.section-5-popup').addClass('active')
                .find('.wrapper')
                .append(videoiFrame);
            //find("iframe")[0].src += "&autoplay=1";
        });
        $('.section-5-popup .close-button').click(function(){
            // Stop youtube iFrame on close
            if($(".section-5-popup").hasClass("active")){
                // Prevent visible jagged iframe
                setTimeout(() => {
                    $('.section-5-popup iframe').remove();
                    //[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
                }, 250);
            }
            $('.section-5-popup').removeClass('active');
        });
        
        $(".section-4 .items").find(".item").each(function(){
            $(this).scrollAnimation();
        });
        $(window).scroll(function(){
            $(".section-4 .items").find(".item").each(function(){
                $(this).scrollAnimation();
            });
        });
    }

    /** Scroll to next section */
    $(".jump-to-top").click(function(){
        $("html, body").animate({ scrollTop: 0}, 500);
    });
});