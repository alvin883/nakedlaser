<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package leenderhof
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

	<nav class="the-navbar" id="the-navbar">
		<div class="container">
			<div class="left">
				<?php if(function_exists('the_custom_logo')) {
					the_custom_logo(); 
				} ?>
			</div>
			<div class="right">

				<div class="hide">
					<span>
						<b>Bel Ons:</b>
						<a href="tel:<?php echo str_replace(' ', '', str_replace('-', '', get_field('telp', 'option'))); ?>">
							<?php the_field('telp', 'option'); ?>
						</a>
					</span>
					<span>
						<b>Email Ons:</b>
						<a href="mailto:<?php the_field('email', 'option'); ?>">
							<?php the_field('email', 'option'); ?>
						</a>
					</span>
				</div>

				<div class="blocking"></div>

				<!-- open button, nav-box wrapper, and close button -->
				<div class="nav-button btn white" tabindex="0">

					<div class="open-menu">
						<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							viewBox="0 0 384.97 384.97" xml:space="preserve"
							class="icon-menu icon-menu-open">
								<path d="M12.03,120.303h360.909c6.641,0,12.03-5.39,12.03-12.03c0-6.641-5.39-12.03-12.03-12.03H12.03
									c-6.641,0-12.03,5.39-12.03,12.03C0,114.913,5.39,120.303,12.03,120.303z"/>
								<path d="M372.939,180.455H12.03c-6.641,0-12.03,5.39-12.03,12.03s5.39,12.03,12.03,12.03h360.909c6.641,0,12.03-5.39,12.03-12.03
									S379.58,180.455,372.939,180.455z"/>
								<path d="M372.939,264.667H132.333c-6.641,0-12.03,5.39-12.03,12.03c0,6.641,5.39,12.03,12.03,12.03h240.606
									c6.641,0,12.03-5.39,12.03-12.03C384.97,270.056,379.58,264.667,372.939,264.667z"/>
						</svg>
					</div>

					<button class="nav-close btn white circle">
						<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							viewBox="0 0 47.971 47.971" xml:space="preserve"
							class="icon-menu icon-menu-close">
							<path d="M28.228,23.986L47.092,5.122c1.172-1.171,1.172-3.071,0-4.242c-1.172-1.172-3.07-1.172-4.242,0L23.986,19.744L5.121,0.88
								c-1.172-1.172-3.07-1.172-4.242,0c-1.172,1.171-1.172,3.071,0,4.242l18.865,18.864L0.879,42.85c-1.172,1.171-1.172,3.071,0,4.242
								C1.465,47.677,2.233,47.97,3,47.97s1.535-0.293,2.121-0.879l18.865-18.864L42.85,47.091c0.586,0.586,1.354,0.879,2.121,0.879
								s1.535-0.293,2.121-0.879c1.172-1.171,1.172-3.071,0-4.242L28.228,23.986z"/>
						</svg>
					</button>

					<div class="nav-box navbar">
						<?php
							$args = array(
								'theme_location' => 'primary',
								'depth'      => 2,
								'container'  => false,
								'menu_class'     => 'navbar-nav',
								'walker'     => new Naked_Custom_Walker()
								);
							if (has_nav_menu('primary')) {
								wp_nav_menu($args);
							}
						?>
						<div class="bottom">
							<div class="contact">
								<div>
									<b>Bel Ons:</b>
									<a href="tel:<?php echo str_replace(' ', '', str_replace('-', '', get_field('telp', 'option'))); ?>">
										<?php the_field('telp', 'option'); ?>
									</a>
								</div>
								<div>
									<b>Email Ons:</b>
									<a href="mailto:<?php the_field('email', 'option'); ?>">
										<?php the_field('email', 'option'); ?>
									</a>
								</div>
							</div>
							<div class="socmed">
								<a href="<?php the_field('facebook', 'option'); ?>" target="_blank" class="btn circle">
									<i class="fab fa-facebook-f"></i>
								</a>
								<a href="<?php the_field('instagram', 'option'); ?>" target="_blank" class="btn circle">
									<i class="fab fa-instagram"></i>
								</a>
							</div>
						</div>
					</div>

				</div>

			</div>
		</div>
	</nav>

<?php /*
	<nav class="navbar navbar-expand-xl fixed-top" id="navbar">
		<div class="top">
			<div class="container">
				<div class="left">
					<i class="icon fas fa-map-marker-alt"></i>
					<?php the_field('adres', 'option'); ?>
				</div>
				<div class="right">
					<a href="tel:<?php the_field('telp', 'option'); ?>">
						<i class="icon fas fa-phone"></i>
						<span class="hide">
							<?php the_field('telp', 'option'); ?>
						</span>
					</a>
				</div>
			</div>
		</div>
		<div class="bottom">
			<div class="container">
				<div class="left-side">
					<?php if (function_exists('the_custom_logo')) {
							the_custom_logo();
					}?>
				</div>
				
				<?php #only appear when screen <= `lg` size ?>
				<div class="right-side-collapse">
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
						<i class="fas fa-bars"></i>
					</button>
					
					<div class="divider facebook-small"></div>
					<a href="https://facebook.com" target="_blank" class="facebook-button facebook-small">
						<i class="fab fa-facebook-f"></i>
					</a>
				</div>

				<div class="right-side navbar-collapse collapse" id="navbarContent">
					<?php
						$args = array(
							'theme_location' => 'primary',
							'depth'      => 2,
							'container'  => false,
							'menu_class'     => 'navbar-nav',
							'walker'     => new Bootstrap_Walker_Nav_Menu()
							);
						if (has_nav_menu('primary')) {
							wp_nav_menu($args);
						}
					?>
				</div>
				
				<?php #only appear when screen > `lg` size ?>
				<div class="divider facebook-large"></div>
				<?php if( get_field('soc_med_fb_link', 'option')) : ?>
					<a href="<?php the_field('soc_med_fb_link', 'option'); ?>" target="_blank" class="facebook-button facebook-large">
						<i class="fab fa-facebook-f"></i>
					</a>
				<?php endif; ?>

			</div>
		</div>
	</nav>
	*/?>